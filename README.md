# soomgo

> Soomgo Assignment

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

---

> ##Project Setting

``` bash
# vue cli 설치
$ npm install -g @vue/cli

# init 형식의 template 가져오기 위해 vue cli init 설치
$ npm install -g @vue/cli-init

# 프로젝트 생성
$ vue init webpack soomgo

# vuex 설치
$ npm install --save -dev vuex
```

> ## `/src` 하위 폴더

1. ###`./`
    - `.eslintrc.js` 파일에 `comma-dangle` 설정 추가

2. ###`/assets`
    - `/styles/common.scss` : 공용 css 적용을 위한 파일
    - `/styles/bootstrap.scss` : bootstrap grid style 적용 위해 bootstrap 4.0 참고 하여 scss 생성
    - `/img` : 이미지 저장 폴더
    - `/inputs.json` : 과제 형식이 명시된 `.json` 파일

3. ###`/components`
    - `/Button.vue` : 공용 Button Component, loading 표시, `class` 별 색상 분리
    - `/Popup.vue` : 공용 Popup Component, `alert` 타입과 `confirm` 타입이 있음. Popup 이 닫힐 때 클릭된 버튼의 값을 `v-on:popupOff` 이벤트를 이용해 받을 수 있음.
    
4. ###`/pages`
    - `/home.vue` : 홈 화면
    - `/request/index.vue` : 각 단계를 보여주기 위한 상위 Component
    - `/request/step1.vue` : 1 단계. 체크박스는 기존 직접 구현한 css 를 이용, 선택된 checkbox 가 없을 경우 validation 통해 팝업 노출.
    - `/request/step2.vue` : 2 단계. 라디오는 기존 직접 구현한 css 를 이용, 선택된 radio 가 없을 경우 validation 통해 팝업 노출.
    - `/request/step3.vue` : 3 단계. input 에 아무 값이 입력되어 있지 않다면 팝업 노출을 통해 통과 할 것인지 선택하도록.
    - `/request/step4.vue` : 4 단계. select 박스 중 선택되지 않은 곳이 있을 경우 팝업 노출.
    - `/request/confirm.vue` : 각 단계를 통해 저장된 값을 모두 보여주고 요청서 제출 전 확인시켜주는 페이지.
    - `/request/result.vue` : 제출 이후 보여지는 페이지.    
    
5. ###`/router`
    - `/index.js` : `vue-router` 를 이용하여 페이지 이동하도록 설정한 파일. [LazyLoading](https://router.vuejs.org/kr/guide/advanced/lazy-loading.html) 에서 참조하여 component 와 page 연결. `beforeEnter` 함수를 통해 `/request` 의 단계에 대한 직접적인 url 접근이 불가하도록 설정. 유효한 경로 이외의 url 로 접근 시 홈으로 redirect 되도록 설정.
    
6. ###`/store` 
    - `index.js` : `Vuex` 설정을 위한 파일, 각 페이지별 정보를 `localstorage` 에 저장하여 최종 페이지에서 가져오기 위함.

---

> ## 과제 결과 확인
- `/request/result` 페이지에서 콘솔 창에 `submitData` 라는 명의 log 확인

---

> ## 해결 못한 문제
1. unit-testing.
2. page 별 meta tag 삽입.
3. scss variable 사용. ex) ``$primary: #00c7ae;``. global 적용이 되지 않음.
4. scss image 경로 문제. ex) `common.scss` 의 `select` 에 적용하는 image 경로가 static 아래 위치해야 적용됨.
