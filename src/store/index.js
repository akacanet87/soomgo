import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const createStore = () => {
  return new Vuex.Store({
    state: {
      form: '',
    },
    mutations: {
      setForm (state, form) {
        state.form = form
        if (form) localStorage.setItem('form', JSON.stringify(form))
        else localStorage.removeItem('form')
      },
    },
    getters: {
      getForm (state) {
        if (localStorage.getItem('form')) return JSON.parse(localStorage.getItem('form'))
        else return state.form
      },
    },
  })
}

export default createStore()
