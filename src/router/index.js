import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/pages/home'

const Request = () => import(/* webpackChunkName: "group-request" */ '@/pages/request')
const Step1 = () => import(/* webpackChunkName: "group-request" */ '@/pages/request/step1')
const Step2 = () => import(/* webpackChunkName: "group-request" */ '@/pages/request/step2')
const Step3 = () => import(/* webpackChunkName: "group-request" */ '@/pages/request/step3')
const Step4 = () => import(/* webpackChunkName: "group-request" */ '@/pages/request/step4')
const Confirm = () => import(/* webpackChunkName: "group-request" */ '@/pages/request/confirm')
const Result = () => import(/* webpackChunkName: "group-request" */ '@/pages/request/result')

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/request',
      name: 'Request',
      component: Request,
      redirect: '/request/step1',
      children: [
        {
          path: 'step1',
          component: Step1,
        },
        {
          path: 'step2',
          component: Step2,
          beforeEnter: (to, from, next) => {
            if (!localStorage.getItem('form') ||
              (localStorage.getItem('form') && localStorage.getItem('form').indexOf('step1') < 0)) {
              next({ name: 'Home' })
            } else {
              next()
            }
          },
        },
        {
          path: 'step3',
          component: Step3,
          beforeEnter: (to, from, next) => {
            if (!localStorage.getItem('form') ||
              (localStorage.getItem('form') && localStorage.getItem('form').indexOf('step2') < 0)) {
              next({ name: 'Home' })
            } else {
              next()
            }
          },
        },
        {
          path: 'step4',
          component: Step4,
          beforeEnter: (to, from, next) => {
            if (!localStorage.getItem('form') ||
              (localStorage.getItem('form') && localStorage.getItem('form').indexOf('step3') < 0)) {
              next({ name: 'Home' })
            } else {
              next()
            }
          },
        },
        {
          path: 'confirm',
          component: Confirm,
          beforeEnter: (to, from, next) => {
            if (!localStorage.getItem('form') ||
              (localStorage.getItem('form') && localStorage.getItem('form').indexOf('step4') < 0)) {
              next({ name: 'Home' })
            } else {
              next()
            }
          },
        },
        {
          path: 'result',
          component: Result,
          beforeEnter: (to, from, next) => {
            if (!localStorage.getItem('form') ||
              (localStorage.getItem('form') && localStorage.getItem('form').indexOf('step4') < 0)) {
              next({ name: 'Home' })
            } else {
              next()
            }
          },
        },
      ],
    },
    { path: '*', redirect: '/' },
  ],
})
